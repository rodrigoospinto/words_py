## Problem description

You have a file containing a long list of words. Write a function that, given any two words, finds the shortest distance between them in terms of numbers of words in between.

For example, if this was the content of the file:
“We do value and reward motivation in our development team. Development is a key skill for a DevOp.”

The value of find_shortest_distance(‘motivation’, ‘development’) should be 2 (“in our”).

It can be case insensitive.

Requirements
 - Use Python 3.6 or higher
 - Include tests and a short documentation on how to run it
 - (bonus) commit your code to GitHub and, ideally, keep it in 2-4 commits so that we can see the history (don’t squash everything in one)

## Running the project.

Dependencies:

- Python 3.6

To make easier to run the project I created a small CLI for the purpose of running the code.

Running the finder with two existing words in the text:

	$ python finder.py --words motivation --words development --file file_fixture.txt

	=> The distance between 'motivation' and 'development' is 2 word(s).

Running the finder with one or both words not existing in the text:

	python finder.py --words foo --words development --file file_fixture.txt

	=> One or both words, 'foo' and 'development', was not found in the text


*The CLI was not a requirement, might be understood as a "big design up front", however, I thought could make easier to run the code against different file content. I usually tend to make simpler solutions, so I could simply have written a script with fixed words and also to read the file directly and call the command: `python run_distance_finder.py`.*

## Solution "Changelog"

Here it is my *brain dump* on how I thought on solving the problem referencing the most important commits.

[89e5d77] I started the solution adding some test cases for scenarios that I have identify and solving the first example given on the problem description.

[24673bf] I treat case when a word did not exist to raise a custom exception.

[daccbd6] Implementing the case where a word appears in different places of the text and find the short distance between the two words.

[c88de3e] and [26adf67] Cleaning up the text to avoid unexpected conditions like ignoring multiple spaces between words.
