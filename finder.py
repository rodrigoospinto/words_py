import argparse
from distance_finder import *

parser = argparse.ArgumentParser()
parser.add_argument('--words', action='append',
                    help="the two words to be searched on the on text")
parser.add_argument('--file', nargs='?', type=argparse.FileType('r'),
                    help="the file where to find the content")

args = parser.parse_args()
file = args.file
word, another_word = args.words

finder = DistanceFinder(file.read())

try:
    distance = finder.find_shortest_distance(word, another_word)
    print("The distance between '{}' and '{}' is {} word(s)\n".format(word, another_word, distance))
except WordNotFoundError:
    print("One or both words, '{}' and '{}', was not found in the text\n".format(word, another_word))

