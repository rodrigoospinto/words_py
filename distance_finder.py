class DistanceFinder:
    def __init__(self, content):
        self.words = self.__normalize(content)
        self.offset = 1

    def find_shortest_distance(self, word_one, word_two):
        word_one_index = None
        word_two_index = None
        min_distance = None

        for i, w in enumerate(self.words):
            if w == word_one:
                word_one_index = i
            if w == word_two:
                word_two_index = i

            if word_one_index != None and word_two_index != None:
                new_distance = self.__distance_for(word_one_index, word_two_index)
                if min_distance == None or new_distance < min_distance:
                    min_distance = new_distance


        if min_distance == None:
            raise WordNotFoundError

        return min_distance

    def __normalize(self, text):
        """
        Converts the `text` string in list of words without empty spaces.
        """
        return text.lower().replace(".", "").split(  )

    def __distance_for(self, position_a, position_b):
        """
        Returns the distance between the two word positions minus a offset to
        adjust the number as a list starts at index 0.
        """
        return abs(position_a - position_b) - self.offset

class WordNotFoundError(Exception):
    pass
