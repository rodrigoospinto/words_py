import unittest
from distance_finder import *

class TestDistanceFinder(unittest.TestCase):
    def setUp(self):
        self.content = "We do value and reward motivation in our development team. Development is a key skill for a DevOp."
        self.finder = DistanceFinder(self.content)

    def test_two_existing_words(self):
        result = self.finder.find_shortest_distance('motivation', 'development')
        self.assertEqual(result, 2)

    def test_word_does_not_exist_in_the_test(self):
        with self.assertRaises(WordNotFoundError):
            self.finder.find_shortest_distance('engineering', 'development')
        with self.assertRaises(WordNotFoundError):
            self.finder.find_shortest_distance('development', 'engineering')

    def test_a_word_next_to_another(self):
        result = self.finder.find_shortest_distance('reward', 'motivation')
        self.assertEqual(result, 0)

    def test_word_appearing_more_than_once(self):
        result = self.finder.find_shortest_distance('development', 'a')
        self.assertEqual(result, 1)

        result = self.finder.find_shortest_distance('development', 'team')
        self.assertEqual(result, 0)

    def test_word_distance_with_many_spaces(self):
        finder= DistanceFinder("We do value       and reward motivation in our development team.")
        result = finder.find_shortest_distance('do', 'reward')
        self.assertEqual(result, 2)


if __name__ == '__main__':
    unittest.main()
